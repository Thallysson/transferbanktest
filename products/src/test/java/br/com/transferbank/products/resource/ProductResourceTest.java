package br.com.transferbank.products.resource;

import br.com.transferbank.products.model.Product;
import br.com.transferbank.products.service.ProductService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductResource.class)
class ProductResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService mockProductService;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

    @Test
    void testPostProduct() throws Exception {
        // Setup

        // Configure ProductService.createOneProduct(...).
        final Product product = new Product("id", 0.0, 0.0, 0, new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone("BET")).getTime().getTime()));
        when(mockProductService.createOneProduct(any(Product.class))).thenReturn(product);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(post("/api/product")
                .content(gson.toJson(product)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(gson.toJson(product).toString());
    }

    @Test
    void testPatchProduct() throws Exception {
        // Setup

        // Configure ProductService.updateOneProduct(...).
        final Product product = new Product("id", 0.0, 0.0, 0, new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone("BET")).getTime().getTime()));
        when(mockProductService.updateOneProduct(eq("id"), any(Product.class))).thenReturn(product);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(patch("/api/product/{id}", "id")
                .content(gson.toJson(product)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(gson.toJson(product).toString());
    }

    @Test
    void testDeleteProduct() throws Exception {
        // Setup

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/product/{id}", "id")
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        verify(mockProductService).deleteOneProduct("id");
    }
}
