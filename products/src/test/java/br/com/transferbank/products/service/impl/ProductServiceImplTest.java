package br.com.transferbank.products.service.impl;

import br.com.transferbank.products.model.Product;
import br.com.transferbank.products.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository mockProductRepository;

    @InjectMocks
    private ProductServiceImpl productServiceImplUnderTest;

    @Test
    void testCreateOneProduct() {
        // Setup
        final Product product = new Product("id", 0.0, 0.0, 0, Date.valueOf(LocalDate.of(2020, 1, 1)));

        // Configure ProductRepository.insert(...).
        final Product product1 = new Product("id", 0.0, 0.0, 0, Date.valueOf(LocalDate.of(2020, 1, 1)));
        when(mockProductRepository.insert(any(Product.class))).thenReturn(product1);

        // Run the test
        final Product result = productServiceImplUnderTest.createOneProduct(product);

        // Verify the results
        assertThat(result.equals(product1));
    }

    @Test
    void testUpdateOneProduct() {
        // Setup
        final Product product = new Product("id", 0.0, 0.0, 0, Date.valueOf(LocalDate.of(2020, 1, 1)));

        // Configure ProductRepository.save(...).
        final Product product1 = new Product("id", 0.0, 0.0, 0, Date.valueOf(LocalDate.of(2020, 1, 1)));
        when(mockProductRepository.save(any(Product.class))).thenReturn(product1);

        // Run the test
        final Product result = productServiceImplUnderTest.updateOneProduct("id", product);

        // Verify the results
        assertThat(result.equals(product1));
    }

    @Test
    void testDeleteOneProduct() {
        // Setup

        // Run the test
        productServiceImplUnderTest.deleteOneProduct("id");

        // Verify the results
        verify(mockProductRepository).deleteById("id");
    }
}
