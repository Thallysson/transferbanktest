package br.com.transferbank.products.service.impl;

import br.com.transferbank.products.model.Product;
import br.com.transferbank.products.repository.ProductRepository;
import br.com.transferbank.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createOneProduct(Product product) {
        return productRepository.insert(product);
    }

    @Override
    public Product updateOneProduct(String id, Product product) {
        product.setId(id);
        return productRepository.save(product);
    }

    @Override
    public void deleteOneProduct(String id) {
        productRepository.deleteById(id);
    }
}
