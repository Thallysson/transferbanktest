package br.com.transferbank.products.service;

import br.com.transferbank.products.model.Product;

public interface ProductService {
    Product createOneProduct(Product product);
    Product updateOneProduct(String id, Product product);
    void deleteOneProduct(String id);
}
