package br.com.transferbank.products.resource;

import br.com.transferbank.products.model.Product;
import br.com.transferbank.products.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API de Produtos")
@CrossOrigin(origins = "*")
public class ProductResource {

    @Autowired
    private ProductService productService;

    @PostMapping(value = "/product")
    @ApiOperation(value = "Cria um produto")
    public Product postProduct(@Valid @RequestBody Product product){
        return productService.createOneProduct(product);
    }

    @PatchMapping(value = "/product/{id}")
    @ApiOperation(value = "Atualiza um produto")
    public Product patchProduct(@Valid @PathVariable String id, @Valid @RequestBody Product product){
        return productService.updateOneProduct(id, product);
    }

    @DeleteMapping(value = "/product/{id}")
    @ApiOperation(value = "Deleta um produto")
    public void deleteProduct(@Valid @PathVariable String id){
        productService.deleteOneProduct(id);
    }
}
